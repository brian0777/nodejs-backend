import { Service } from 'typedi'; // Inject
import { getManager } from 'typeorm'; // getRepository
import IUserBusiness from './interfaces/IUserBusiness'; // eslint-disable-line
import UserView from '../models/user/UserView';
// import { RoleCode } from '../models/common/Enum';
import ResultList from '../models/common/ResultList';


@Service()
export default class UserBusiness implements IUserBusiness {
    private entityManager: any = getManager();

    async findUsers(keyword?: string, skip?: number, limit?: number): Promise<ResultList<UserView>> {
        const resultList = new ResultList<UserView>(skip, limit);
        // let query = createQueryBuilder('user')
        //     .innerJoinAndSelect('user.role', 'role', 'role.deletedAt IS NULL')
        //     .where('user.deletedAt IS NULL');

        // if (keyword && keyword.trim()) {
        //     keyword = `%${keyword.trim()}%`;
        //     query = query.andWhere(new Brackets(qb => {
        //         qb.where(`user.firstName || ' ' || user.lastName ILIKE :keyword`, { keyword })
        //             .orWhere(`user.email ILIKE :keyword`, { keyword });
        //     }));
        // }
        // let [users, count] = await query
        //     .skip(resultList.pagination.skip)
        //     .take(resultList.pagination.limit)
        //     .getManyAndCount();

        // resultList.results = UserView.parseArray(users);
        // resultList.pagination.total = count;
        return resultList;
    }

    async findAll(): Promise<any> {
        let test = await this.entityManager.query(`exec GetClassById @ClassId=126`);
        return test;
    }

    async getUserByToken(token: string): Promise<UserView | undefined> {
        // if (!token)
        //     throw new ValidationError();

        // const user = await this.userRepository.createQueryBuilder('user')
        //     .innerJoinAndSelect('user.role', 'role', 'role.deletedAt IS NULL')
        //     .where('user.deletedAt IS NULL')
        //     .andWhere('user.accessToken = :token', { token })
        //     .andWhere('user.tokenExpire >= NOW()')
        //     .cache(token, 10 * 60 * 1000)
        //     .getOne();

        // if (user && (!user.tokenExpire || new Date(user.tokenExpire) < new Date())) {
        //     await PostgreAccess.removeCaching(token);
        //     return undefined;
        // }
        // return user && new UserView(user);
        return undefined;
    }
};
