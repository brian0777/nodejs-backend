// import AssessmentView from '../../models/assessment/AssessmentView';
// import AssessmentCreate from '../../models/assessment/AssessmentCreate';
// import AssessmentUpdate from '../../models/assessment/AssessmentUpdate';
// import ResultList from '../../models/common/ResultList';

interface IAssessmentBusiness {
    sendEmailAssessment(): Promise<Boolean>;

    // findAssessments(keyword?: string, skip?: number, limit?: number): Promise<ResultList<AssessmentView>>;

    // getAssessment(id: number): Promise<AssessmentView | undefined>;

    // getAssessmentByCode(code: number): Promise<AssessmentView | undefined>;

    // createAssessment(data: AssessmentCreate): Promise<AssessmentView | undefined>;

    // updateAssessment(id: number, data: AssessmentUpdate): Promise<boolean>;

    // deleteAssessment(id: number): Promise<boolean>;

    // initialAssessments(data: {isRequired?: boolean, data: any}[], isRequired?: boolean): Promise<boolean>;
}

export default IAssessmentBusiness;
