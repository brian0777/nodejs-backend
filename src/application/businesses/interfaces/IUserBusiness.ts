import UserView from '../../models/user/UserView';
import UserProfile from '../../models/user/UserProfile'; // eslint-disable-line
import ResultList from '../../models/common/ResultList';

interface IUserBusiness {
    findUsers(keyword?: string, skip?: number, limit?: number): Promise<ResultList<UserView>>;
    findAll(): Promise<any>;
    getUserByToken(token: string): Promise<UserView | undefined>;
}

export default IUserBusiness;
