import * as path from 'path';
import { Service } from 'typedi'; // Container,
import { getManager } from 'typeorm'; // Repository
// import { Validator } from 'class-validator';
// import { InjectRepository } from 'typeorm-typedi-extensions';
import IAssessmentBusiness from './interfaces/IAssessmentBusiness'; // eslint-disable-line
// // import Assessment from '../entities/Assessment';
// import AssessmentView from '../models/assessment/AssessmentView';
// import AssessmentCreate from '../models/assessment/AssessmentCreate';
// import AssessmentUpdate from '../models/assessment/AssessmentUpdate';
// import { CommonError } from '../models/common/Error';
// import ResultList from '../models/common/ResultList';
// import DataHelper from '../../helpers/DataHelper';
import MailHelper from '../../helpers/MailHelper';
import FileHelper from '../../helpers/FileHelper';
// const validator = Container.get(Validator);

@Service()
export default class AssessmentBusiness implements IAssessmentBusiness {
    // @InjectRepository(Assessment)
    // private AssessmentRepository: Repository<Assessment>;
    private static entityManager: any = getManager();

    async sendEmailAssessment(): Promise<Boolean> {
        let test: any = FileHelper.readFile(path.join(__dirname, `../../resources/templates/mail/test.html`), 'utf8');
        MailHelper.sendMail('thi.nguyen@starmathsonline.com.au', 'test', undefined, test);
        return true;
    }

    // async findAssessments(keyword?: string, skip?: number, limit?: number): Promise<ResultList<AssessmentView>> {
    //     const resultList = new ResultList<AssessmentView>(skip, limit);

    //     let query = this.entityManager.createQueryBuilder('assessment')
    //         .where('assessment.deletedAt IS NULL');

    //     if (keyword && keyword.trim())
    //         query = query.andWhere('assessment.name ilike :keyword', { keyword: `%${keyword.trim()}%` });

    //     let [assessments, count] = await query
    //         .orderBy({
    //             name: 'ASC'
    //         })
    //         .skip(resultList.pagination.skip)
    //         .take(resultList.pagination.limit)
    //         .getManyAndCount();

    //     resultList.results = AssessmentView.parseArray(assessments);
    //     resultList.pagination.total = count;
    //     return resultList;
    // }

    // async getAssessment(id: number): Promise<AssessmentView | undefined> {
    //     DataHelper.validateId(id);

    //     const assessment = await this.AssessmentRepository.createQueryBuilder('assessment')
    //         .whereInIds([id])
    //         .andWhere('assessment.deletedAt IS NULL')
    //         .getOne();

    //     return assessment && new AssessmentView(assessment);
    // }

    // async getAssessmentByCode(code: number): Promise<AssessmentView | undefined> {
    //     if (!code)
    //         throw new CommonError(101, 'code');
    //     if (!validator.isInt(code))
    //         throw new CommonError(102, 'code');

    //     const assessment = await this.AssessmentRepository.createQueryBuilder('assessment')
    //         .where('assessment.code = :code', { code })
    //         .andWhere('assessment.deletedAt IS NULL')
    //         .getOne();

    //     return assessment && new AssessmentView(assessment);
    // }

    // private async getAssessmentByName(name: string, excludeId?: number): Promise<AssessmentView | undefined> {
    //     if (!name)
    //         throw new CommonError(101, 'name');

    //     let query = this.AssessmentRepository.createQueryBuilder('assessment')
    //         .where('lower(assessment.name) = :name', { name: name.trim().toLowerCase() })
    //         .andWhere('assessment.deletedAt IS NULL');

    //     if (excludeId)
    //         query = query.andWhere('assessment.id != :id', { id: excludeId });

    //     const assessment = await query.getOne();
    //     return assessment && new AssessmentView(assessment);
    // }

    // async createAssessment(data: AssessmentCreate): Promise<AssessmentView | undefined> {
    //     await DataHelper.validateDataModel(data);

    //     if (await this.getAssessmentByCode(data.code))
    //         throw new CommonError(105, 'code');

    //     const assessmentExists = await this.getAssessmentByName(data.name);
    //     if (assessmentExists)
    //         throw new CommonError(105, 'name');

    //     const result = await this.AssessmentRepository.insert(data);

    //     let assessment;
    //     if (result.raw && result.raw.length)
    //         assessment = await this.getAssessment(result.raw[0].id);
    //     return assessment;
    // }

    // async updateAssessment(id: number, data: AssessmentUpdate): Promise<boolean> {
    //     await DataHelper.validateModel(id, data);

    //     const assessment = await this.AssessmentRepository.createQueryBuilder('assessment')
    //         .whereInIds([id])
    //         .andWhere('assessment.deletedAt IS NULL')
    //         .getOne();

    //     if (!assessment)
    //         throw new CommonError(104, 'assessment');

    //     const assessmentExists = await this.getAssessmentByName(data.name, id);
    //     if (assessmentExists)
    //         throw new CommonError(105, 'name');

    //     await this.AssessmentRepository.update(id, data);
    //     return true;
    // }

    // async deleteAssessment(id: number): Promise<boolean> {
    //     DataHelper.validateId(id);

    //     const assessment = await this.AssessmentRepository.createQueryBuilder('assessment')
    //         .whereInIds([id])
    //         .andWhere('assessment.deletedAt IS NULL')
    //         .getOne();

    //     if (!assessment)
    //         throw new CommonError(104, 'assessment');

    //     await this.AssessmentRepository.update(id, { deletedAt: new Date() });
    //     return true;
    // }

    // async initialAssessments(list: {isRequired?: boolean, data: any}[], isRequired?: boolean): Promise<boolean> {
    //     if (!list || !Array.isArray(list))
    //         throw new CommonError();

    //     for (let i = 0; i < list.length; i++) {
    //         const item = list[i];
    //         if (item.isRequired || isRequired) {
    //             const assessment = await this.getAssessmentByCode(item.data.code);
    //             if (!assessment) {
    //                 const assessmentCreate = new AssessmentCreate();
    //                 assessmentCreate.code = item.data.code;
    //                 assessmentCreate.name = item.data.name;

    //                 await this.createAssessment(assessmentCreate);
    //             }
    //         }
    //     }
    //     return true;
    // }
};
