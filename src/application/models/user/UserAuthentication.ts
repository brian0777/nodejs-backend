import UserProfile from './UserProfile';

export default class UserAuthentication {
    id: number;
    profile: UserProfile;
    accessToken?: string;
    tokenExpire?: Date;

    constructor(data: any) {
        this.id = data.id;
        this.profile = new UserProfile(data);
        this.accessToken = data.accessToken;
        this.tokenExpire = data.tokenExpire;
    }
};
