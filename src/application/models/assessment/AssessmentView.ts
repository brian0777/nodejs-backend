// import Assessment from '../../entities/Assessment';

export default class AssessmentView {
    id: number;
    code: number;
    name: string;
    createdAt: Date;
    updatedAt: Date;

    constructor(data: any) {
        this.id = data.id;
        this.code = data.code;
        this.name = data.name;
        this.createdAt = data.createdAt;
        this.updatedAt = data.updatedAt;
    }

    static parseArray(list: any[]): AssessmentView[] {
        return list && Array.isArray(list) ? list.map(item => new AssessmentView(item)) : [];
    }
};
