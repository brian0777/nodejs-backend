// import Assessment from '../../entities/Assessment';

export default class AssessmentReference {
    id: number;
    code: number;
    name: string;

    constructor(data: any) {
        this.id = data.id;
        this.code = data.code;
        this.name = data.name;
    }

    static parseArray(list: any[]): AssessmentReference[] {
        return list && Array.isArray(list) ? list.map(item => new AssessmentReference(item)) : [];
    }
};
