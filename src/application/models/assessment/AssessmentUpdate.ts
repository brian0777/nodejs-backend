import { MaxLength, IsString } from 'class-validator';

export default class AssessmentUpdate {
    @IsString()
    @MaxLength(50)
    name: string;
};
