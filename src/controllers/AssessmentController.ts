import { Service, Inject } from 'typedi';
import { JsonController, Get } from 'routing-controllers';
import IAssessmentBusiness from '../application/businesses/interfaces/IAssessmentBusiness';
import AssessmentBusiness from '../application/businesses/AssessmentBusiness';
// import AssessmentCreate from '../application/models/assessment/AssessmentCreate';
// import AssessmentUpdate from '../application/models/assessment/AssessmentUpdate';

@Service()
@JsonController('/assessments')
export default class AssessmentController {
    @Inject(() => AssessmentBusiness)
    private assessmentBusiness: IAssessmentBusiness;

    @Get('/')
    findAssessments() {
        return this.assessmentBusiness.sendEmailAssessment();
    }

    // @Get('/')
    // @Authorized(AssessmentClaim.GET)
    // findAssessments(@QueryParam('keyword') keyword: string, @QueryParam('skip') skip: number, @QueryParam('limit') limit: number) {
    //     return this.assessmentBusiness.findAssessments(keyword, skip, limit);
    // }

    // @Get('/:id([0-9]+)')
    // @Authorized(AssessmentClaim.GET)
    // getAssessment(@Param('id') id: number) {
    //     return this.assessmentBusiness.getAssessment(id);
    // }

    // @Get('/assessment-by-code')
    // @Authorized(AssessmentClaim.GET)
    // getAssessmentByCode(@QueryParam('code') code: number) {
    //     return this.assessmentBusiness.getAssessmentByCode(code);
    // }

    // @Post('/')
    // @Authorized(AssessmentClaim.CREATE)
    // createAssessment(@Body() data: AssessmentCreate) {
    //     return this.assessmentBusiness.createAssessment(data);
    // }

    // @Put('/:id([0-9]+)')
    // @Authorized(AssessmentClaim.UPDATE)
    // updateAssessment(@Param('id') id: number, @Body() data: AssessmentUpdate) {
    //     return this.assessmentBusiness.updateAssessment(id, data);
    // }

    // @Delete('/:id([0-9]+)')
    // @Authorized(AssessmentClaim.DELETE)
    // deleteAssessment(@Param('id') id: number) {
    //     return this.assessmentBusiness.deleteAssessment(id);
    // }
};
